import {
    ADD_TODO,
    TOGGLE_TODO,
    DELETE_TODO,
    ADD_INPUT_VALUE,
    SHOW_DONE,
    SHOW_UNDONE,
    SHOW_ALL
} from '../actions';


const postsInitialState = {
    allData: [],
    renderData: [],
    valueInput: ''
};

const todoReducer = (state = postsInitialState, action) => {
    switch (action.type) {

        case ADD_TODO:
            const { valueInput } = state
            const newItem = {
                task: valueInput,
                done: false,
                id: new Date().getTime()
            }

            return {
                ...state,
                allData: [...state.allData, 
                    newItem
                ],
                renderData: [...state.renderData, newItem]
            }

        case ADD_INPUT_VALUE:
            console.log(action);
         
            return {
                ...state,
                valueInput: action.payload
            }


        case TOGGLE_TODO:
            let todos = state.renderData.map(item => {
                if (item.id === action.payload) {
                    item.done = !item.done;
                }
                return item;
            });

            return {
                
                ...state,
                renderData: todos
                
            }


        case DELETE_TODO:
            let newtodos = state.allData.filter(item => {
                if (item.id !== action.payload) {
                    return item;
                }

            });

            return {
                ...state,
                allData: newtodos,
                renderData: newtodos
            }

        case SHOW_DONE:

        return {
            ...state,
            renderData: state.allData.filter(item => item.done)
        }

        case SHOW_UNDONE:
        return {
            ...state,
            renderData: state.allData.filter(item => !item.done)
        }

        case SHOW_ALL:
        return {
            ...state,
            renderData: state.allData
        }
        

        default:
            return state;
    }
}

export default todoReducer;