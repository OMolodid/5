import App from './components/app';
import NotFound from './NotFound';


 const RootRoutes = [
    {
        title: 'Main',
        path: '/',
        component: App,
        exact: true
    },
    {
        title: 'All things to do',
        path: '/all',
        component: App,
        exact: true
    },
    {
        title: 'Done',
        path: '/done',
        component: App,
        exact: true
        
    },
    {
        title: 'Undone',
        path: '/undone',
        component: App,
        exact: true
       
    },
   
    {
        component: NotFound
    }

]

export default RootRoutes;