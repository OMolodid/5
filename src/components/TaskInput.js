import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import { addInputValue} from '../actions';


class TaskInput extends Component {
    changeHandler = (e) => {
        const { addInputValue } = this.props;

        addInputValue(e.target.value);
    }

    render = () => {
        let { type, placeholder, value } = this.props;
        
        let { changeHandler } = this;


        return (
            <>
            <input
            type={type}
            value={value}
            onChange={changeHandler}
            placeholder={placeholder}
        />
        </>

        )

    }

}


TaskInput.propTypes = {


    placeholder: PropTypes.string,
    handler: PropTypes.func,
    type: PropTypes.oneOf(['text', 'password', 'number']),
    value: PropTypes.oneOfType([
 PropTypes.string,
 PropTypes.any,
]),
};


const mapDispatchToProps = (dispatch) => ({
    addInputValue: (valueInput) => {
        dispatch(addInputValue(valueInput));
 
    }
});

export default connect(null, mapDispatchToProps)(TaskInput);

