import React from 'react';
import { connect } from 'react-redux';
import TaskInput from './TaskInput';
import ToDoItem from './ToDoItem';


import { addTodo, toggleTodo, deleteTodo, showDone, showUndone, showAll } from '../actions';

class App extends React.Component {

    componentDidMount() {
        const { location, showDone, showUndone, showAll } = this.props;
        switch (location.pathname) {
            case '/done':
            showDone()
                break;

            case '/undone':
            showUndone()
                 break;

            case '/all':
            showAll()
                 break;
        
            default:
                break;
        }

        
    }

    addToStore = () => {
        const { addTodo } = this.props;
         addTodo();
    }

    toggleTodo = (id) => () => {
        this.props.toggleTodo(id);
    }

    deleteTodo = (id) => () => {
        this.props.deleteTodo(id);
    }

    showDone = () => () => {
        this.props.showDone();
    }
    showUndone = () => () => {
        this.props.showUndone();
    }
    showAll = () => () => {
        this.props.showAll();
    }

    render() {
        const { addToStore, toggleTodo, deleteTodo } = this;
        const { todos, valueInput } = this.props;


        console.log(todos);
        

        return (
            <div>
               
                <header>
                    <TaskInput
                        type="text"
                        value={valueInput}
                        placeholder="Type new task"
                    />

                    <button onClick={addToStore}> Add Task </button>
                </header>

                <ul className='list'>
                    {
                        todos.map(todo => {
                            return (
                                <ToDoItem
                                    key={todo.id}
                                    item={todo}
                                    action={toggleTodo}
                                    type="checkbox"
                                    buttonaction={deleteTodo}
                                />
                            )
                        })
                    }
                </ul>
            </div>
        )
    }
}

/*
    Redux
*/
const mapStateToProps = (state, ownProps) => ({
    todos: state.todos.renderData,
})

const mapDispatchToProps = (dispatch) => ({
    addTodo: () => {
        dispatch(addTodo());
            },
    toggleTodo: (id) => {
        dispatch(toggleTodo(id));
            },
    deleteTodo: (id) => {
        dispatch(deleteTodo(id));
            },
     showDone: (allData) =>{
        dispatch(showDone(allData));
            },
    showUndone: (allData) =>{
        dispatch(showUndone(allData));
            },
    showAll: (allData) =>{
        dispatch(showAll(allData));
              },
            
});
        
export default connect(mapStateToProps, mapDispatchToProps)(App);