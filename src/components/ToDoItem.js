import React from 'react';

const ToDoItem = ({ item, action, type, buttonaction }) => (

    <li key={item.id}
    className='list-item'
    >
        <input
     
            checked={item.done ? true : false}
            type={type}
            onChange={action(item.id)}
        />
        <span>{item.task}</span>
        <button
            onClick={buttonaction(item.id)}
        >
            Delete
        </button>
    </li>

)



// const mapDispatchToProps = (dispatch) => ({
//     addTodo: (
//         // newTodo
//         ) => {
//         dispatch(addTodo(
//             // newTodo
//             ));
//     },
//     toggleTodo: (id) => {
//         dispatch(toggleTodo(id));
//     },
//     deleteTodo: (id) => {
//         dispatch(deleteTodo(id));
//     }
// });

// export default connect(mapStateToProps, mapDispatchToProps)(ToDoItem);



export default ToDoItem;

