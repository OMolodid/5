export const ADD_TODO = 'ADD_TODO';
export const TOGGLE_TODO = 'TOGGLE_TODO';
export const DELETE_TODO = 'DELETE_TODO';
export const ADD_INPUT_VALUE = 'ADD_INPUT_VALUE';
export const SHOW_DONE = 'SHOW_DONE';
export const SHOW_UNDONE = 'SHOW_UNDONE';
export const SHOW_ALL = 'SHOW_ALL';

export const addTodo = (data) => (dispacth) => {
    dispacth({
        type: ADD_TODO,
        payload: data
    });
}

export const addInputValue = (valueInput) => (dispacth) => {
    dispacth({
        type: ADD_INPUT_VALUE,
        payload: valueInput
    });
}

export const toggleTodo = (id) => (dispacth) => {
    dispacth({
        type: TOGGLE_TODO,
        payload: id
    });
}

export const deleteTodo = (id) => (dispacth) => {
    dispacth({
        type: DELETE_TODO,
        payload: id
    });
} 
export const showDone = (data) => (dispacth) => {
    dispacth({
        type: SHOW_DONE,
        payload: data
    });
}
export const showUndone = (data) => (dispacth) => {
    dispacth({
        type: SHOW_UNDONE,
        payload: data
    });
}
export const showAll = (data) => (dispacth) => {
    dispacth({
        type: SHOW_ALL,
        payload: data
    });
}