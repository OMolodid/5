import React from 'react';
import { Provider } from 'react-redux';
import {
	BrowserRouter,
	Route
} from 'react-router-dom';
// import App from './components/app';
import App from './App';
import store from './redux/store';

import './App.css';

const Root = () => {
	return (
		<>
			<Provider store={store}>
				<BrowserRouter>
					<Route path="/" component={App} />
				</BrowserRouter>
			</Provider>
		</>
	);
}

export default Root;

