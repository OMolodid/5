import React from 'react';
import Navigation from './navigation';
import { 
  Switch,
  Route
} from 'react-router-dom';
import rootRoutes from './rootRoutes';
import './App.css';


function App(props) {
  return (
    <div className='wrap'>
    <div className='navigation'><Navigation /></div>
    <Switch key={props.location.key}>
      {
        rootRoutes.map((route, key) => {
          return (
            <Route key={key} {...route} />
          )
        })
      }
    </Switch>

  </div>
  );
}

export default App;
